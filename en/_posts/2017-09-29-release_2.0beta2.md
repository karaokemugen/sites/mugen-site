---
layout: post
title:  "Releasing KM version 2.0 Beta 2 ~Finé Foutraque~"
date:   2017-09-29 12:16:44
language: en
handle: /2017/09/29/lancement_2.0beta2.html
---

After more than ten days correcting several dozens of bugs, the second beta of Karaoke Mugen is available !

![alley looya]({{ site.baseurl }}/images/articles/1300962431128.gif){: .center-image }

* [Download Karaoke Mugen 2.0 Beta 2]({{ siteurl.baseurl }}/en/download.html)
* [Karaoke Mugen 2.0 Beta 2 Source Code](https://gitlab.com/karaokemugen/code/karaokemugen-app/repository/v2.0-beta2/archive.zip)

You can [view the changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags/v2.0-beta2)

For more detailed installation instructions, please reach the [complete documentation](https://docs.karaokes.moe/en/) !
You can also find updating instructions on the [version 2.0 Beta 2 page, on Shelter's Gitlab](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags/v2.0-beta2), if you had already installed the first beta.

The final 2.0 version is expected for early November, and will be titled **Finé Fantastique** !

Once again, your feedback matters ! This application is made for YOU. Your feelings and ideas are important to enhance the application, but we also need your reports to help us identifying and fixing bugs.
[Contact us !]({{ site.baseurl }}/en/contact.html)
