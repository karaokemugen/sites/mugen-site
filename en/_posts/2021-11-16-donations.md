---
layout: post
title:  "Donate to Karaoke Mugen"
date:   2021-11-16 06:00:00
language: en
handle: /2021/11/16/donations.html
---

We have been sometimes asked about it, but we never really needed to receive donation from people who enjoyed Karaoke Mugen. We like money, like everyone else acutally, but we're doing this for the passion of it before anything else, and if the main Karaoke Mugen team is only made of a few people, the number of people who participate, no matter how, to the project can be counted by the tens.

Thanks to you all, we wouldn't be here today without you.

![gif]({{ site.baseurl }}/images/articles/1hr8z9.gif){: .center-image }

That said, Karaoke Mugen is being hosted right now on Shelter, a small community server which today struggles with its load. Karaoke Mugen is a part of that load, and since its ressources are limited, we came to two decisions :

- First we'll move away from [Shelter's lab](https://lab.shelter.moe) to be hosted on [Gitlab.com](https://gitlab.com) for our repositories. They have an Open Source Program which will allow us to be hosted there for free, if everything goes well.
- Second, we need to self-host our own sites, app, and services so not to bother Shelter anymore.

If the first point doesn't need any financial aid, for the second one we don't have much choice and will need to rent a server in a datacenter.

The topic of accepting money donations now comes back on the table.

## Why donate ?

As said just now, managing Karaoke Mugen's infrastructure for the software needs ressources, so we'll need a dedicated server, which costs between 50 and 100€ depending on what we'll get with the donations we'll receive.

Of course, since these donations without a goal, this means that we have possible plans that we can achieve depending on how much money we get. Some examples :

- Print flyers we could distribute at anime events.
- Get an Apple developer account to sign our macOS app and perhaps even consider an iOS app.
- Put bounties on some bugs or features we can't do, either by lack of time or knowledge.
- Commission more artwork from [Sedeto](http://sedeto.fr), who designed our logo and Nanamin
- Perhaps we could get more ideas later?

Your money can be used for that and will be on a dedicated Paypal account.

## How to donate ?

Thanks for reading this far.

We have three ways to donate, that you can find here :

### Patreon

<a href="https://www.patreon.com/bePatron?u=64513113" data-patreon-widget-type="become-patron-button">{{ site.f_patreon[page.language] }}</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>

### Paypal

<form action="https://www.paypal.com/donate" method="post" target="_top">
	<input type="hidden" name="hosted_button_id" value="2FGU9X24PPGFS" />
    <input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Bouton Faites un don avec PayPal" />
    <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1" />
</form>

If the button doesn't work, [try this link](https://www.paypal.com/donate/?hosted_button_id=YTZKDQWQS5R4J).

### Liberapay

<script src="https://liberapay.com/karaokemugen/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/karaokemugen/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

## Why 3 ways to donate ?

We thought some people would like Patreon better because it's easier to use and manage your subscriptions, while other people would like to use Paypal better because they have an account there. Some dislike both and would thus prefer Liberapay, etc.Note that these three payment methods will end up on our Paypal account anyway. However, they'll allow you to use a credit card instead of your Paypal account no matter what you choose.

We have things to offer via Patreon, because that's easier to do, but we won't be able to offer them for those who pay directly through Paypal or Liberapay because that'll be harder for us ot manage. We are really few working on Karaoke Mugen and we have to manage our time. The less time we spend on micromanaging stuff like that, the more we can spend on fixing bugs and improving the app and its services.

Thanks for understanding :)

If you have questions, don't hesitate to [contact us](/en/contact.html). And if you can't donate money and wish to help us in any other way, you can help us by [participating](/en/participate.html) !
