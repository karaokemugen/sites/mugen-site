---
layout: post
title:  "Karaoke Mugen 2.3.1"
date:   2018-08-22 06:00:00
language: fr
handle: /2018/08/22/KM231.html
---

Karaoke Mugen 2.3.1 « Ichika Insouciante » est sortie ! Il s'agit d'une version corrigeant de nombreux bugs repérés sur la 2.3.0, dont certains très gênants; Et ici on aime pas les bugs gênants. Personne n'aime les bugs gênants.

![gif]({{ site.baseurl }}/images/articles/htae1894rz.gif){: .center-image }

**IMPORTANT : Le fichiers .kara version 2 ou plus bas sont obsolètes. Merci de mettre à jour votre base de karaokés.**

## Téléchargements

Comme d'hab, [direction la page de téléchargements]({{site.baseurl}}/download.html)

## Améliorations

- Les recherches prennent en compte le nom original d'une série.
- Les karas en erreur ne sont plus ajoutés à votre base de donées lors d'une régénération.
- Les fichiers audios sont acceptés lors de la création d'un kara.
- Beaucoup d'améliorations et d'optimisations ont été faites à la base de données pour récupérer la liste des karaokés ou le contenu d'une playlist. Merci à @Jaerdoster et ses *mad skillz* en SQL.
- Ajout d'un tag XBOXONE.
- mpv n'essaye plus d'auto-charger des fichiers externes, ce qui veut dire de meilleures performances à la lecture d'une vidéo sur un lecteur réseau.

### Correctifs

- Le bouton de mise à jour de la base de karaokés refonctionne. C'était embarrassant.
- La modification d'un karaoké hardsubbé fonctionne.
- Les noms des fichiers sont mieux générés lors de l'ajout/modification d'un karaoké
- La recherche dans les playlists fonctionne de nouveau.
- Colmatage de possibles injections SQL.
- Quand un media n'est plus présent, lire les détails d'un karaoké n'échouera plus.
- Correction de traductions anglaises.
- Les jingles fonctionnent de nouveau !
- Sur macOS, le log n'est plus spammé de notifications comme quoi le fichier de configuration a changé.
- Le fichier de configuration pouvait parfois être écrasé avec un fichier vide, ça a normalement été réparé.
- Les chansons sont maintenant bien supprimées automatiquement de la playlist publique une fois jouées.

Comme d'habitude, si vous trouvez d'autres bugs ou avez des questions, n'hésitez pas à [nous contacter]({{site.baseurl}}/contact.html)