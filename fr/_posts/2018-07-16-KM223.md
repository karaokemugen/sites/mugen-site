---
layout: post
title:  "Karaoke Mugen 2.2.3"
date:   2018-07-16 06:00:00
language: fr
handle: /2018/07/16/KM223.html
---

Karaoke Mugen 2.2.3 « Haruhi Hyperactive » est disponible ! C'était ça ou Hirsute, mais on a pas trouvé d'image.

![gif]({{ site.baseurl }}/images/articles/heaa.gif){: .center-image }

Il s'agit d'une version mineure qui corrige quelques bugs.

## Téléchargements

Comme d'hab, [direction la page de téléchargements]({{site.baseurl}}/download.html)

## Correctifs

- #332 Les téléchargements via le système de MAJ intégrée à l'app devraient fonctionner chez tout le monde et ne plus bloquer l'application.
- Correction d'un gros bug avec la (ré)generation de la base de données des séries, qui pouvait causer des noms de séries complètement délirants dans la liste des karaokés.
- Karaoke Mugen ne devrait plus planter lorsqu'on essaye de générer une base de karas sans .karas. Y'a des fous j'vous jure.
- Les apostrophes dans les noms de série sont maintenant bien détectés pour les recherches.
- Les médias sont maintenant téléchargés en premier lors des MAJ depuis l'app.

Si vous avez des questions, ou besoin d'aide, n'hésitez pas à [nous contacter]({{site.baseurl}}/contact.html) !
