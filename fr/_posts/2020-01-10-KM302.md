---
layout: post
title:  "Karaoke Mugen 3.0.2 !"
date:   2020-01-10 06:00:00
language: fr
handle: /2020/01/10/KM302.html
---

Karaoke Mugen 3.0.2 « Leafa Langoureuse » vous fait les yeux doux !

![gif]({{ site.baseurl }}/images/articles/ngzoiu.gif){: .center-image }

Il s'agit d'une version corrective. Vous pouvez l'installer sans danger.

Elle corrige encore pas mal de petits problèmes qu'on a repéré avec la 3.0.1, mais là on est très très confiants et ça devrait être la dernière 3.0.x.

## Téléchargements

Comme d'hab, [direction la page de téléchargements.]({{site.baseurl}}/download.html)

## Mettre à jour votre installation

Décompressez simplement l'archive téléchargée dans le dossier de Karaoke Mugen pour remplacer les fichiers.

## Nouveautés de cette version

### Améliorations

- **Sur mobile, l'ajout de chanson ou le passage à la playlist ne se font plus par un swipe, mais par des boutons.** Le swipe causait trop de problèmes, d'ajouts par erreur et avait du mal à suivre sur pas mal d'appareils.
- Le code de sécurité ne peut plus être utilisé pour réinitialiser votre mot de passe local. Si vous avez perdu votre mot de passe, utilisez le code de sécurité pour vous créer un nouveau compte opérateur.
- Le gagnant d'un sondage est envoyé au chat de Twitch quand disponible.
- La configuration est correctement mise à jour lorsqu'on affiche la page de config dans le panneau système.
- En mode restreint, l'interface n'affichera une explication que si vous êtes sur mobile.
- Les apostrophes ne sont plus retirées durant les recherches. « May'n » cherchera bien « May'n », et pas « May » et « n ».
- Un message s'affichera pour vous dire de vérifier si une chanson n'est pas disponible au téléchargement avant d'en suggérer l'ajout à l'équipe Karaoke Mugen.
- Amélioration de la façon dont l'autorisation d'un utilisateur est faite.
- L'icône pour dire si une chanson est cachée ou non est maintenant cliquable.
- Sur le formulaire d'ajout de karaoké, le champ des séries recherche maintenant dans les alias et les noms internationaux.

### Correctifs

- Correction du paramètre autoplay qui fonctionne maintenant comme convenu.
- Lors du téléchargement d'une chanson, les tags et séries pouvaient avoir besoin d'être supprimés s'ils avaient changé de nom, ça pouvait causer une erreur et donc une annulation du téléchargement de la chanson, alors que ça ne devrait pas poser de problème (c'est souvent parce que le tag a déjà été supprimé).
- Correction du tag Série TV des samples.
- Correction de l'erreur « Le surnom ne peut être vide » quand on modifie son mot de passe.
- Correction du tutorial opérateur.
- Correction du bug des playlists invisibles.
- Toyunda2ASS a été mis à jour en version 1.0.8 et détecte correctement les sauts de ligne Windows.
- Les pourcentages des votes du sondage sont maintenant arrondis à deux décimales.
- Les sondages devraient être sondamentalement OK maintenant.
- Lorsqu'on passe de la liste des chansons à une liste à filtres (en appliquant un filtre), le placement du scroll est réinitialisé.
- Le temps restant d'une playlist est maintenant bien mis à jour.
- Il n'y a plus de clignotement lorsqu'on défile dans une playlist !
- Correction du défilement dans la liste des utilisateurs connectés.
- Correction de l'ajout d'une IP dans le champ « IP personnalisé » de la page Configuration du panneau Système.
- Correction des boîtes de dialogue modales sur les petits écrans.
- Correction du rendu initial des playlists.
- Correction de l'affichage des favoris sur la page publique.
- Correction de l'alignement du karaoké en cours de lecture au début de la playlist.
- Correction de l'ouverture du modal de connexion quand on se déconnecte.
- Correction du spam de toasts lorsque la page était cachée ou que l'appareil était en veille.
- Correction du mode restreint.
- Correction de l'affichage du type de chanson sur mobile lorsque le titre fait plusieurs lignes.
- Correction de la couleur des boutons sur la boîte de détails d'une chanson.
- Correction de l'affichage de la boîte d'aide.
- Correction de la mise à jour et des filtres des chansons sur la page de téléchargement de chansons.

Pour une liste plus complète (en anglais) vous pouvez lire [le fichier CHANGELOG](https://gitlab.com/karaokemugen/code/karaokemugen-app/blob/v3.0.2/CHANGELOG.md).
