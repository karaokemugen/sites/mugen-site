---
layout: post
title:  "Karaoke Mugen 2.4.1 ! [IMPORTANT]"
date:   2018-11-28 06:00:00
language: fr
handle: /2018/11/28/KM24.html
---

Karaoke Mugen 2.4.1 « Juri Joviale » se dévoile, et il y a deux grosses nouveautés IMPORTANTES !

![gif]({{ site.baseurl }}/images/articles/00d.gif){: .center-image }

## Nouveau format de nom de fichiers

**IMPORTANT : ** Cette version vous permet de renommer tous vos médias au nouveau standard que nous utilisons. Comme nous allons renommer tous les médias dans notre base de karaokés, si vous mettez à jour votre propre base **SANS RENOMMER AU PREALABLE VOS MEDIAS** vous allez devoir retélécharger 90% de la base environ lors de votre prochaine mise à jour. Et vous ne voulez pas que ça arrive, hein ?

Afin d'éviter ça :

* Cette opération n'est à faire qu'une seule fois.
* **Dans la nuit du samedi 1er décembre au dimanche 2 décembre, après 00h00** (nous renommerons notre base entière à cette date) :
* Lancez Karaoke Mugen 2.4.1
* Allez dans le panneau **Système**
* Allez sur l'onglet **Database**
* Cliquez sur **Rename all media files** et confirmez.
* Vérifiez la progression dans la console (ça prend 2 minutes).
* Une fois le renommage terminé, **mettez à jour votre base de karas normalement**.

Si vous avez des questions, n'hésitez pas à [nous soliciter]({{ site.baseurl }}/contact.html)

## Envoi de statistiques d'utilisation

Depuis qu'on a lancé **Karaoké Mugen**, certaines stats d'utilisation sont crées dans les bases de données des instances mais jamais envoyées car nous n'avions pas d'endroit où les recueillir.

Nous travaillons dur sur **Karaoke Mugen Server**, qui va proposer plusieurs fonctionnalités utiles (un meilleur *listing* des chansons que sur le site, un moyen de participer à un karaoké sans être sur le même réseau Wifi, etc.) Parmi ces fonctionnalités, afficher des stats intéressantes a été l'un de nos objectifs :

* Les chansons favorites
* Les chansons les plus demandées
* Les chansons les plus vues

Nous avions besoin d'un moyen d'avoir ces stats, mais pour ça votre instance de **Karaoke Mugen** doit les envoyer à notre serveur.

Le paquet contenant les stats est plutôt petit (environ 100 Ko pour une grosse instance)

Quand vous lancerez **Karaoke Mugen 2.4.1** pour la première fois, vous aurez le choix entre accepter ou non de nous envoyer ces stats.

Ce choix peut être changé à tout moment dans les options.

Les stats sont envoyées à chaque démarrage et à chaque heure que l'application tourne. Elle sont complètement anonymisées (nous saurons combien de fois un karaoké a été mis en favori mais pas par qui, par exemple) et vous pouvez voir le paquet qui est envoyé en entier dans le fichier de log de **Karaoke Mugen**.

Les stats seront publiées en temps réel sur **Karaoke Mugen Server** bientôt(tm).

Nous aimerions vraiment proposer aux gens des infos intéressantes sur les sessions de **Karaoke Mugen**, et cela commence avec ces quelques stats.

Nous espérons que vous accepterez de nous les envoyer.

Si vous avez des questions, n'hésitez pas à [nous soliciter]({{ site.baseurl }}/contact.html)

## Téléchargements

Comme d'hab, [direction la page de téléchargements]({{site.baseurl}}/download.html)

### Laissez-vous séduire par ces nouvelles fonctionnalités innovantes(tm)

- Les stats sont maintenant uploadées de façon périodique sur **Karaoke Mugen Server** (si l'admin d'instance est d'accord) ([#377](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/377))
- La procédure de renommage des médias est disponible dans le panneau système pour permettre aux gens d'éviter de retélécharger tous les médias ([#376](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/376))
- Les petits slogans d'initalisation(tm) sont maintenant affichés sur l'écran d'accueil ([#378](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/378))

### Correctifs

- Glisser & Déplacer des chansons dans une playlist ne marchait parfois pas comme prévu. Les positions des karaokés sont maintenant bien prises en compte ([#375](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/375))
- Correction de la création des automix.
- La fenêtre du moniteur n'est plus synchronisée avec le lecteur vidéo principal, car cela causait des comportements bizarres sur certaines vidéos car le moniteur essayait de rattraper son retard (ou son avance).
- Les messages d'erreur bizarres sur la configuration invalide n'apparaîtront plus ([#373](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/373))
