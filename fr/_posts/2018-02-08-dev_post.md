---
layout: post
title:  "Developpement en cours 2"
date:   2018-02-08 10:00:00
language: fr
handle: /2018/02/08/dev_post.html
---

Le temps passe vite, cela fait déjà ~~un mois~~ deux mois que nous avions fait un billet sur le développement de **Karaoke Mugen** !

J'espère que vous vous amusez bien avec le logiciel et sa base.

Nous continuons à l'améliorer, mais nous n'avons pas donné beaucoup de nouvelles. Ce post est là pour y remédier.

...Ce que je viens d'écrire vous est familier parce que, heh, il n'y avait rien de plus à dire.

# Avancement

Depuis mon retour du japon, le développement a repris son cours normal. Comme il y a deux mois, les deux chantiers sont les mêmes mais je vais vous donner des nouvells :

Nous visons une sortie de la 2.1 *Gabriel Glamoureuse* pour début Mars, mais il n'est pas impossible que cela arrive plus tard.

# Sur `master`

Pas de nouvelles, nous n'avons pas eu de remontée de bug demandant des soins urgents !

# Sur `next`

Des changements massifs sous le capot de la version expérimentale de **Karaoke Mugen** :

- La gestion des utilisateurs est bien entamée mais a encore besoin de quelques finitions. Il est désormais nécessaire de s'identifier pour utiliser l'app via son interface Web, mais n'ayez pas peur, on a voulu garder ça très simple d'emploi. Comme je l'ai déjà expliqué [par le passé](https://mugen.karaokes.moe/fr/2017/12/12/etat_dev.html), la gestion des utilisateurs est une fondation nécessaire pour beaucoup de nouvelles fonctions du logiciel.

- Le logiciel a été entièrement réécrit en ES2015+ mais pour le moment la branche `next` est encore bugguée et a besoin de tests approfondis de notre part. Au omins, le plus dur est fait !

# Karaoke Mugen se propage !

Nous recevons de temps en temps des salutations de gens qui utilisent **Karaoke Mugen** pour organiser leurs soirées karaoke et nous les en remercions, ça fait toujours plaisir de voir que notre travail peut servir. En rendant KM et sa base open source, c'était exactement ce que l'on voulait : que chacun l'utilise à sa guise et puisse participer.

C'est ce que font certains déjà, comme le [Bakaclub de l'ENSIIE](http://www.ensiie.fr/vie-etudiante/vie-associative/article/bakaclub) qui contribue beaucoup à notre base et offre des retours sur le logiciel, mais aussi des étudiants de [l'EISTI de Cergy] qui ont déjà commencé à apporter des karaokes à la base. Cela fait plaisir de voir différentes associations en plus des utilisateurs de **Karaoke Mugen** participer de temps en temps au projet.

Que la communauté otaku en France participe à un projet commun de karaoke est ce qui nous a motivé en premier lieu. Nous souhaitons que tout le monde puisse se servir de **Karaoke Mugen** et participer à son développement, en code logiciel comme en contenu.

A noter que **Karaoke Mugen** fera sa grande première en convention lors de [Jonetsu 3.33 !](http://jonetsu.fr). Jonetsu est un évènement autour des métiers et de la création de l'animation et du manga. Deux séances de karaoke seront prévues durant les deux jours, mais l'intêret principal de Jonetsu réside dans ses conférences passionnantes et son panel d'artistes ayant chacun leur stand dans la convention. Si vous n'avez rien de prévu le **week-end des 7-8 Avril 2018** et que vous pouvez venir à **Bourg-La-Reine** (dans le 92), c'est un super bon plan !

Bon karaokes à vous tous !
