---
layout: post
title:  "Karaoke Mugen 2.5.1 !"
date:   2019-05-06 06:00:00
language: fr
handle: /2019/05/06/KM251.html
---

Karaoke Mugen 2.5.1 « Konata Kiffante », en avant !

Il s'agit d'une version corrective. La version précédente, 2.5.0, était victime de quelques bugs gênants que nous avons corrigés.

![gif]({{ site.baseurl }}/images/articles/bb07943896b4bae2a54325ba8c0ccf74a46d8d6dr1-500-281_hq.gif){: .center-image }

## Téléchargements

Comme d'hab, [direction la page de téléchargements.]({{site.baseurl}}/download.html)

## Changements

### Correctifs

- Ajout d'un texte expliquant comment se connecter au panneau système avec un compte en ligne.
- Correction de l'ajout / suppression de tags sur les critères de blacklist.
- Correction de l'import / export des favoris depuis l'interface admin et publique.
- Correction de l'import des critères de blacklist depuis une vieille base de données SQLite.
- Correction du bug des téléchargements qui ne démarraient pas automatiquement au démarrage de l'application.
- Correction de l'interrupteur de session karaoké qui ne voulait plus repasser en "privé" après l'avoir passé en "public".
- Correction de la mise à jour des profils en ligne, tout est désormais normalement bien synchronisé.
- Correction de la boîte modale du mail de suggestion de nouveau karaoké.
- Correction des erreurs avec les karaokés multi-séries.
