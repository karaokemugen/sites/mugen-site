---
layout: post
title:  "Karaoke Mugen Server, qu'est-ce que c'est ?"
date:   2018-12-13 07:00:00
language: fr
handle: /2018/12/13/KMServer.html
---

Hier nous avons mis en production un nouvel explorateur de karaokés pour parcourir la base de **Karaoke Mugen**. Avec plus de 6400 karaokés, il fallait bien un moyen simple pour vous de chercher votre chanson préférée.

[Cette nouvelle version de la base de karaokés se trouve ici](https://kara.moe)

![gif]({{ site.baseurl }}/images/articles/kifaevtac.gif){: .center-image }

Le site que vous visiterez en cliquant sur le lien ci-dessus fait partie d'un site plus complet qui est encore en cours de développement, et il s'appelle **Karaoke Mugen Server**.

**KM Server** fait déjà :

- Réception et centralisation des statistiques envoyés par les applications Karaoke Mugen
- Raccourcisseur d'URL pour votre instance (le fameux kara.moe que vous tapez pour utiliser votre KM)
- Explorateur de la base de karaokés

Et à terme, la version 1.0 va proposer :

- Un moyen pour l'app Karaoke Mugen de parcourir la base de karaokés et télécharger uniquement les karaokés que vous voulez (plus besoin de tout télécharger).
- Un affichage des stats récupérées.
- Un système de comptes et de favoris en ligne, afin que vous puissiez trimballer votre liste de favoris facilement d'une instance à l'autre.
- Un mode en ligne pour Karaoke Mugen qui vous permettra de faire un karaoké avec des gens sans qu'ils soient sur le même réseau wifi que vous.
- Un moyen de nous envoyer facilement vos karaokés via un formulaire à remplir.

Et peut-être d'autres choses plus tard.

## C'est pour qui du coup ?

**Karaoke Mugen Server** étant un projet libre s'interfaçant avec l'application **Karaoke Mugen**, vous pouvez, en tant que personne, association ou autre, gérer votre propre **Karaoke Mugen Server**, avec votre propre base de karaokés !

L'app **Karaoke Mugen** peut en effet dialoguer avec le **KM Server** de son choix : si vous avez un super nom de domaine que vous préférez gérer vous-même comme par exemple `genshi.ken` ou `pony.tail` et y proposer vos propres karaokés dessus, vous pourrez le faire : les applications **KM** pourront s'y connecter.

Si par exemple on refuse un karaoké qui vous plait bien pour une raison X ou Y, vous pourrez sans souci gêrer votre propre base et proposer à d'autres tous vos super karaokés de Michel Sardou.

Bref, les utilisateurs de l'application ne sont pas contraints d'utiliser le serveur de `kara.moe`.

Si l'idée vous intéresse, n'hésitez pas [à nous contacter]({{site.baseurl}}/contact.html) pour qu'on en parle. [Aider au développement](https://gitlab.com/karaokemugen/code/karaokemugen-server) pourrait aussi grandement nous aider.