---
layout: post
title:  "Karaoke Mugen 6.1 !"
date:   2022-03-27 06:00:00
language: fr
handle: /2022/03/27/KM61.html
---

Karaoke Mugen 6.1 « Quintuplées Qualitatives » allie qualité ET quantité !

![gif]({{ site.baseurl }}/images/articles/bd4.gif){: .center-image }

Il s'agit d'une version mineure de Karaoke Mugen avec un changement de taille et quelques changements et correctifs bienvenus.

## Téléchargements

Téléchargez la dernière version sur [la page des téléchargements.]({{site.baseurl}}/download.html)

## Mettre à jour votre installation

### Windows

Karaoke Mugen devrait vous proposer une mise à jour automatique, sinon vous pouvez le mettre à jour manuellement :

- Si vous choisissez la version par installeur, vous n'avez qu'à l'exécuter et pointer vers le chemin de votre choix.
- Si vous choisissez la version portable, sachez que **les mises à jour automatiques de l'application ne seront pas possibles depuis celle-ci**. Décompressez simplement l'archive dans votre dossier Karaoke Mugen et lancez l'exécutable possédant une icône Karaoke Mugen.

### macOS

Pour macOS, installez l'image DMG comme n'importe quelle application Mac. Pour ouvrir l'application, vous devrez faire clic droit puis « Ouvrir » dans le menu. L'application n'étant pas signée (nous n'avons pas (encore ?) les moyens pour cela), macOS vous le fera savoir, à vous de nous faire confiance (ou non).

Si vous déposez Karaoke Mugen dans le dossier Applications, l'app cherchera ses données dans votre dossier utilisateur. Si c'est dans un autre dossier (celui de votre ancienne installation par exemple) assurez-vous qu'un fichier `portable` existe, sans extension. Ainsi, Karaoke Mugen cherchera ses fichiers de données dans ce dossier à la place.

### Linux

Pour Linux, des instructions spécifiques sont disponibles dans la documentation, [merci de la consulter](https://docs.karaokes.moe).

## Nouveautés de cette version

### Nouvelles fonctionnalités étincelantes !

- Collections de chansons : Gérer plusieurs dépôts individuellement a toujours comporté son petit lot de soucis en terme d'infrastructure. Nous avons donc fusionné les deux dépôts World et Otaku dans un seul, et ajouté un nouveau type de tag appelé "Collections". Les collections sont des ensembles de chansons et peuvent être parcourus et utilisés comme tous les autres tags. Elles peuvent être activées ou désactivées par l'opérateur de karaoké au niveau du système s'il ne souhaite pas de ces chansons. Par exemple s'il choisit de désactiver le dépôt « Shitpost », personne ne verra les chansons liés à cette collection.

<div class="flexbox">
    <img src="/images/articles/collections.jpg" alt="home" class="image-left">
</div>

- Les tags peuvent maintenant être synchronisés d'un dépôt à un autre.

- Les automatismes de création de tags permettent d'ajouter des alias de titre sur une chanson selon les titres de celle-ci.

- Les favoris peuvent maintenant être affiché du plus récent au plus ancien (note : on parle de leur ajout dans Karaoke Mugen, pas du moment où vous avez ajouté une chanson à vos favoris).

<div class="flexbox">
    <img src="/images/articles/triefavoris.jpg" alt="home" class="image-left">
</div>

- L'anglais n'est plus considéré comme la langue universelle par défaut lorsqu'il s'agit de décider dans quel langue une série ou un titre de karaoké devrait être affiché. Les chansons ont maintenant un paramètre « Langue du titre par défaut ».

<div class="flexbox">
    <img src="/images/articles/titledefault.jpg" alt="home" class="image-left">
</div>

- Les prévisualisations de karaokés utiliseront maintenant des hardsubs. Ces derniers sont générés par Erin, notre serveur dédié, et seront utilisés lorsque vous cliquez sur « Prévisualiser » sur la fiche d'un karaoké afin que le rendu des paroles soit correct (impossible à faire en softsub sur navigateur).

- Les playlists peuvent maintenant être mélangées _entièrement_, pas juste ce qu'il y a après la chanson en cours de lecture.

- Karaoke Mugen est maintenant compatible avec les modules ECMAScript !

## Corrections

- Correction des favoris qui étaient groupés avec leur parent. Désormais toutes les chansons sont visibles, sans regroupement.

- Correction des noms d'invités cassés, dû au changement de notre bibliothèque de « slug ».

- Correction de la fonction de modification de chanson dans d'autres dépôts, qui créeait des tags à tort.

- Correction de la prévisualisation des automatismes lorsqu'on édite une chanson.

- Correction des messages d'erreur lorsqu'on tente de supprimer une liste de lecture courante/publique/blanche/noire.

- Correction des parents qui apparaissaient plus d'une fois dans la même chanson.

Pour plus de détails, [consultez le changelog complet (en anglais)](https://gitlab.com/karaokemugen/code/karaokemugen-app/-/releases).